var express = require("express");
var router = express.Router();
var db = require("../modules/db")();

router.post("/designation", function(req, res) {
  db.many("SELECT id,name FROM public.designation;")
    .then(designations => {
      res.status(200).send(designations);
    })
    .catch(error_min => {
      console.log(error_min.code);
      var errorobj = {
        error_code: error_min.received,
        error_msg: error_min.message
      };
      res.status(200).send(errorobj);
    });
});

router.post("/all_package", function(req, res) {
  db.many(
    "SELECT package_name,job_post_price,test_given_price,can_taken_price,amount,credit_limit,validity FROM public.package;"
  )
    .then(packages => {
      res.status(200).send(packages);
    })
    .catch(error_min => {
      console.log(error_min.code);
      var errorobj = {
        error_code: error_min.received,
        error_msg: error_min.message
      };
      res.status(200).send(errorobj);
    });
});

router.post("/industry", function(req, res) {
  db.many("SELECT id,name FROM public.industry;")
    .then(industries => {
      res.status(200).send(industries);
    })
    .catch(error_min => {
      console.log(error_min);
      var errorobj = {
        error_code: error_min.received,
        error_msg: error_min.message
      };
      res.status(200).send(errorobj);
    });
});

router.post("/skills", function(req, res) {
  db.many("SELECT id,name FROM public.skills;")
    .then(skills => {
      res.status(200).send(skills);
    })
    .catch(error_min => {
      console.log(error_min.code);
      var errorobj = {
        error_code: error_min.received,
        error_msg: error_min.message
      };
      res.status(200).send(errorobj);
    });
});

router.post("/country", function(req, res) {
  db.many("SELECT id,name FROM public.country;")
    .then(countries => {
      res.status(200).send(countries);
    })
    .catch(error_min => {
      console.log(error_min.code);
      var errorobj = {
        error_code: error_min.received,
        error_msg: error_min.message
      };
      res.status(200).send(errorobj);
    });
});

router.post("/state", function(req, res) {
  let query = {};
  query.id = req.body.country_id;

  if (query.id != null) {
    db.many("SELECT id,name FROM public.state WHERE country_id=${id};", query)
      .then(states => {
        res.status(200).send(states);
      })
      .catch(error_min => {
        console.log(error_min.code);
        var errorobj = {
          error_code: error_min.received,
          error_msg: error_min.message
        };
        res.status(200).send(errorobj);
      });
  } else {
    res.status(400).send("You must give country_id field!");
  }
});

router.post("/city", function(req, res) {
  let query = {};
  query.id = req.body.state_id;

  if (query.id != null) {
    db.many("SELECT id,name FROM public.city WHERE state_id=${id};", query)
      .then(cities => {
        res.status(200).send(cities);
      })
      .catch(error_min => {
        console.log(error_min.code);
        var errorobj = {
          error_code: error_min.received,
          error_msg: error_min.message
        };
        res.status(200).send(errorobj);
      });
  } else {
    res.status(400).send("You must give state_id field!");
  }
});

router.post("/degree", function(req, res) {
  db.many(
    "SELECT degree.id, degree.degree_name,degree.short FROM public.degree;"
  )
    .then(packages => {
      res.status(200).send(packages);
    })
    .catch(error_min => {
      console.log(error_min.code);
      var errorobj = {
        error_code: error_min.received,
        error_msg: error_min.message
      };
      res.status(200).send(errorobj);
    });
});

router.post("/stream", function(req, res) {
  db.many("SELECT stream.id, stream.title,stream.details FROM public.stream;")
    .then(packages => {
      res.status(200).send(packages);
    })
    .catch(error_min => {
      console.log(error_min.message);
      var errorobj = {
        error_code: error_min.received,
        error_msg: error_min.message
      };
      res.status(200).send(errorobj);
    });
});

router.post("/city_details", function(req, res) {
  let query = {};
  query.id = req.body.city_id;

  if (query.id != null) {
    db.one(
      "SELECT e1.id AS city_id,e1.name AS city_name,e2.id AS state_id,e2.name AS state_name,e3.id AS country_id, e3.name AS country_name FROM city e1,state e2,country e3  WHERE e1.id=${id} AND e1.state_id=e2.id AND e2.country_id=e3.id;",
      query
    )
      .then(city => {
        res.status(200).send(city);
      })
      .catch(error_min => {
        console.log(error_min.code);
        var errorobj = {
          error_code: error_min.received,
          error_msg: error_min.message
        };
        res.status(200).send(errorobj);
      });
  } else {
    res.status(400).send("You must give city_id field!");
  }
});

router.post("/blank_table_row", function(req, res) {
  let query = {};
  query.table = req.body.table;

  if (query.table != null) {
    db.many(
      "select column_name from information_schema.columns where table_name = ${table} AND table_schema='public';",
      query
    )
      .then(columns => {
        result = {};
        columns.forEach(column => {
          result[column.column_name] = "";
        });
        res.status(200).send(result);
      })
      .catch(error_min => {
        console.log(error_min.code);
        var errorobj = {
          error_code: error_min.received,
          error_msg: error_min.message
        };
        res.status(200).send(errorobj);
      });
  } else {
    res.status(400).send("You must give table field!");
  }
});

router.post("/hobby", function(req, res) {
  db.many("SELECT * FROM public.hobby;")
    .then(packages => {
      res.status(200).send(packages);
    })
    .catch(error_min => {
      console.log(error_min.message);
      var errorobj = {
        error_code: error_min.received,
        error_msg: error_min.message
      };
      res.status(200).send(errorobj);
    });
});

router.post("/challenge", function(req, res) {
  db.many("SELECT * FROM public.challenge;")
    .then(packages => {
      res.status(200).send(packages);
    })
    .catch(error_min => {
      console.log(error_min.message);
      var errorobj = {
        error_code: error_min.received,
        error_msg: error_min.message
      };
      res.status(200).send(errorobj);
    });
});

router.post("/mothertongue", function(req, res) {
  db.many("SELECT * FROM public.mothertounge;")
    .then(packages => {
      res.status(200).send(packages);
    })
    .catch(error_min => {
      console.log(error_min.message);
      var errorobj = {
        error_code: error_min.received,
        error_msg: error_min.message
      };
      res.status(200).send(errorobj);
    });
});

router.post("/question_type", function(req, res) {
  db.many("SELECT * FROM public.type;")
    .then(packages => {
      res.status(200).send(packages);
    })
    .catch(error_min => {
      console.log(error_min.message);
      var errorobj = {
        error_code: error_min.received,
        error_msg: error_min.message
      };
      res.status(400).send(errorobj);
    });
});

router.get("/add_job", function(req, res) {
  res.status(200).send({
    status: 0,
    apiUsageInfo: {
      request: {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        headers: {
          "Content-Type": "application/json"
        },
        redirect: "follow",
        body: {
          userId: "integer value - Unique User ID",
          name: "Software Developer",
          availableUpto: "2020-03-31",
          url: "https://techmagnox.com/jobs/apply?name=software_developer",
          bannerImage:
            "https://spin.atomicobject.com/wp-content/uploads/garden_pi_code_blurred.png",
          description:
            "Magnox Technologies Pvt. Ltd. \\n\\nResponsibilities:\\nBuild awesome softwares!\\n\\nExperience Required:\\nJust enough.\\nCTC: Rs. XXXXXX pa."
        }
      }
    },
    response: {
      success: {
        status: 1,
        id: "integer value - Unique Job ID"
      },
      error: {
        status: 0
      }
    }
  });
});
router.post("/add_job", function(req, res) {
  let query = {};
  query.user_id = req.body.userId;
  query.name = req.body.name;
  query.desc = req.body.description;
  query.banner_image = req.body.bannerImage;
  query.to = req.body.availableUpto;
  query.url = req.body.url;

  if (
    query.user_id != null &&
    query.name != null &&
    query.desc != null &&
    query.to != null
  ) {
    query.query =
      'INSERT INTO public.challenge (user_id,name,"desc","banner_image",end_date_time,url,category)' +
      " VALUES (${user_id},${name},${desc},${banner_image},${to},${url},4) RETURNING id";

    if (query.query != null)
      db.tx(t => {
        return t
          .one(query.query, query)
          .then(result => {
            result.status = 1;
            return result;
          })
          .catch(error_min => {
            var ret = { status: 0 };
            console.log(error_min);
            return ret;
          });
      }).then(ret => {
        res.status(200).send(ret);
      });
    else res.status(400).send("Operation not permitted.");
  } else {
    res.status(400).send({
      status: 0,
      error: "Error Invalid HTTP Payload.",
      apiUsageInfo: {
        request: {
          method: "POST",
          mode: "cors",
          cache: "no-cache",
          headers: {
            "Content-Type": "application/json"
          },
          redirect: "follow",
          body: {
            userId: "integer value - Unique User ID",
            name: "Software Developer",
            availableUpto: "2020-03-31",
            url: "https://techmagnox.com/jobs/apply?name=software_developer",
            bannerImage:
              "https://spin.atomicobject.com/wp-content/uploads/garden_pi_code_blurred.png",
            description:
              "Magnox Technologies Pvt. Ltd. \n\nResponsibilities:\nBuild awesome softwares!\n\nExperience Required:\nJust enough.\nCTC: Rs. XXXXXX pa."
          }
        }
      },
      response: {
        success: {
          status: 1,
          id: "integer value - Unique Job ID"
        },
        error: {
          status: 0
        }
      }
    });
  }
});

router.post("/get_candidate_details", function(req, res) {
  let query = {};
  query.user_id = req.body.canId;

  if (query.user_id != null) {
    db.tx(t => {
      return t
        .one("SELECT * FROM public.user_details WHERE id=${user_id};", query)
        .then(result => {
          return t
            .oneOrNone(
              "SELECT resume_url FROM b2c_user_resume WHERE user_id=${user_id} AND status=TRUE;",
              query
            )
            .then(result2 => {
              if (result2 != null) result.resume_url = result2.resume_url;
              else result.resume_url = null;
              result.status = 1;
              return result;
            })
            .catch(error_min => {
              var ret = { status: 0 };
              console.log(error_min);
              return ret;
            });
        })
        .catch(error_min => {
          var ret = { status: 0 };
          console.log(error_min);
          return ret;
        });
    })
      .then(ret => {
        res.status(200).send(ret);
      })
      .catch(error_min => {
        console.log(error_min);
        res.status(500).send("internal error!");
      });
  } else {
    res.status(400).send({
      status: 0,
      error: "Error Invalid HTTP Payload.",
      apiUsageInfo: {
        request: {
          method: "POST",
          mode: "cors",
          cache: "no-cache",
          headers: {
            "Content-Type": "application/json"
          },
          redirect: "follow",
          body: {
            canId: "integer value - Unique Candidate ID"
          }
        }
      },
      response: {
        success: {
          id: "4",
          first_name: "Aniruddha",
          last_name: "Sarkar",
          photo_sm: "public/image/profile_pic/sm/new-20190117502156779.jpg",
          photo_lg: "./public/image/profile_pic/lg/Magnox8122-logo.png",
          dateofbirth: "1998-01-03T18:30:00.000Z",
          gender: "M",
          maritalstatus: "Bachelor",
          email: "sarkar4540@gmail.com",
          alt_email: "sarkar4540@india.com",
          phone: "7548950804",
          alt_phone: "123456",
          bloodgroup: "A+",
          aadhar_no: "263269208663",
          website: "",
          address: "73, Abinash Chandra Banerjee Lane",
          pin: "700010",
          facebook_link: "",
          linkedin_link: "",
          google_link: null,
          mothertongue_id: "1",
          about_me: "Struggling towards economic freedom!",
          resume_heading: "Software Engineer",
          expected_ctc: "360000.00",
          caste: "Schedule Caste",
          physical_challenge: false,
          percentage_ph: "0",
          passport_no: "-",
          fathers_name: "Netai Chandra Sarkar",
          fathers_occupation: "Medical Officer",
          mothers_name: "Tapati Sarkar",
          mothers_occupation: "-",
          created_date_time: null,
          modified_date_time: null,
          city_id: "4",
          resume_url: null,
          status: 1
        },
        error: {
          status: 0
        }
      }
    });
  }
});

router.post("/get_candidate_list", function(req, res) {
  let query = {};
  query.user_id = req.body.userId;
  query.job_id = req.body.jobId;

  if (query.user_id != null && query.job_id != null) {
    query.query =
      "SELECT e3.id FROM public.challenge e1 LEFT JOIN public.challenge_apply e2 ON (e1.id=e2.cha_id) RIGHT JOIN public.user_details e3 ON (e2.can_id=e3.id) WHERE e1.id=${job_id} AND e1.user_id=${user_id} AND e1.category=4;";

    if (query.query != null)
      db.tx(t => {
        return t
          .many(query.query, query)
          .then(result => {
            return result;
          })
          .catch(error_min => {
            var ret = { status: 0 };
            console.log(error_min);
            return ret;
          });
      }).then(ret => {
        res.status(200).send(ret);
      });
    else res.status(400).send("Operation not permitted.");
  } else {
    res.status(400).send({
      status: 0,
      error: "Error Invalid HTTP Payload.",
      apiUsageInfo: {
        request: {
          method: "POST",
          mode: "cors",
          cache: "no-cache",
          headers: {
            "Content-Type": "application/json"
          },
          redirect: "follow",
          body: {
            userId: "integer value - Unique User ID",
            jobId: "integer value - Unique Job ID"
          }
        }
      },
      response: {
        success: [{ id: "integer value - Unique Candidate ID" }],
        error: {
          status: 0
        }
      }
    });
  }
});

module.exports = router;
