var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var publicRouter = require("./routes/public");
var privateRouter = require("./routes/private");
var bodyParser = require("body-parser");
var fileUpload = require('express-fileupload');

var cors = require("cors");

var app = express();

var whitelist = [
  "http://localhost",
  "http://localhost:3001",
  "http://18.224.31.58",
  "http://192.168.10.174",
  "http://newb2c.billionskills.com",
  "http://www.newb2c.billionskills.com",
  "http://3.15.187.121"
];
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (whitelist.indexOf(req.header("Origin")) !== -1) {
    corsOptions = { origin: true }; // reflect (enable) the requested origin in the CORS response
  } else {
    corsOptions = { origin: false }; // disable CORS for this request
  }
  callback(null, corsOptions); // callback expects two parameters: error and options
};

app.enable("trust proxy");
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.use(cors(corsOptionsDelegate));
app.use(logger("dev"));
app.use(fileUpload());
app.use(bodyParser.json({ limit: '2mb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use("/uploads", express.static("uploads"));
app.use("/", indexRouter);
app.use("/public", publicRouter);
app.use("/private", privateRouter);

app.use(function (req, res, next) {
  res.status(400).send("You seem to be confused about your doings!");
});

// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.content = err.message;
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

module.exports = app;
